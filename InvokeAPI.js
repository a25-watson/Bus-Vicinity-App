/** 
 * * This is the server executable. 
 * * @module server 
 * * @version 1.0
 * * @author Aiden, Hagen, Dawid <a.watsom1996@yahoo.co.uk>
 * */

// load the express framework
var express = require('express');
var ws = require('./wslib');

// start a new instance
var app = express();
exports.app = app;

/** 
 * * Web service options.
 * * @var {object} connection
 * */
var conf = require('./config/dev.json');
var options = conf.options;
exports.options = options;

/**
  * Add path to options
  * @param {string} p A path.
  * @return {Promise} Options object.
  * @author Aiden, Hagen, Dawid <a.watsom1996@yahoo.co.uk>
  */
function path(p) {
    options.path = p;
    return options;
}

exports.path = path;

app.set('view engine', 'ejs');

/**
* Renderng and path to importsources data on bristol api
*/
app.get("/page1", function (request, response) {
    ws.invoke(path("/api/2.0/static/importsources"),
    (err,data) => {
        //console.log(JSON.stringify(data));
        if (err) response.status(404).send("Oops!");
        else response.render('page1', data);
    });
});

/**
* Renderng and path to importSource
*/
app.get("/page2", function (request, response) {
    var importSource = request.query.importsources; //request.query.importsources;
    ws.invoke(path("/api/2.0/static/routes/info/Source?importSource=" + importSource),
    (err,data) => {
        //console.log(JSON.stringify(data));
        if (err) response.status(404).send("Oops!");
        else response.render('page2', data);
    });
});

/**
* Renderng and path to roueID data
*/
app.get("/page3", function (request, response) {
    var routeID = request.query.routeID; //request.query.routeID;
    ws.invoke(path("/api/2.0/static/trips?routeID=" + routeID),
    (err,data) => {
        //console.log(JSON.stringify(data));
        if (err) response.status(404).send("Oops!");
        else response.render('page3', data);
    });
});

if (process.env.NODE_ENV!="test") {
    app.listen(8080);
    console.log('Listening on port 8080');
}
/** 
 * * This is the Web-Service client library. 
 * * @module client 
 * * @version 1.0
 * * @author Aiden, Hagen, Dawid <a.watsom1996@yahoo.co.uk>
 * */
var https = require('https');

// rate limiter to avoid "API calls quota exceeded! maximum admitted per Second."
var RateLimiter = require('limiter').RateLimiter;
var limiter = new RateLimiter(1, 'second');

function invoke(options, errback) {
    limiter.removeTokens(1, () => {
        try {
            // asynchronous GET
            https.get(options, response => {
                // initialise streamed response
                var body = "";
                // add event listeners to response
                response.on('data', d => body += d);
                response.on('end', () => {
                    errback(null, JSON.parse(body));        
                }); 
            });
        }
        catch (error) { errback(error); }
    })
}

exports.invoke = invoke;
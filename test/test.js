
/** 
 * * This is the test document. 
 * * @module testing 
 * * @version 1.0
 * * @author Aiden, Hagen, Dawid <a.watsom1996@yahoo.co.uk>
 * */

 // testing libraries 
process.env.NODE_ENV = 'test';
var test = require('unit.js');
var jsdom = require('jsdom');
var assert = require('assert');
var { JSDOM } = jsdom;
var server = require('../InvokeAPI');

// options suite
describe("options suite", function() {
    // testing options connecting to API
    it('test options', function() {
        var p = "http://example.com";
        var q = server.path(p);
        test.object(server.options)
            .hasProperty("host", "bristol.api.urbanthings.io")
            .hasProperty("headers")
            .hasProperty("accept", "application/json")
            .hasProperty("path",p)
        assert.equal(server.options.host, "bristol.api.urbanthings.io");
    });

    // testing page2 gathering imortsources data
    it('test page2', function(done) {
        this.timeout(50000);
        //setTimeout(done, 50000);
        test.httpAgent(server.app)
        .get('/page2?importsources=TNDS')
        .set('Accept', 'text/html')
        .expect(200)
        .end(function(error, response) {
            if (error) {
                return done(error.message);
            } else {
                var dom = new JSDOM(response.text);
                test.object(dom.window.document);
                var agencies = dom.window.document.querySelector("select");
                assert.ok(agencies.length > 0);
                done();
            }
        });
    });
});